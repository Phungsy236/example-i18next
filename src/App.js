import React from 'react';
import logo from './logo.svg';
import './App.css';
import { withTranslation, Trans } from 'react-i18next';

function App(props) {
 

  const { t, i18n } = props;
  // you can choose language to display via i18n.changeLanguage , if don't i18next will use default language is vi
  //i18n.changeLanguage('en');
  return (
    <div className='App'>
      <button
        onClick={() => {
          i18n.changeLanguage('en');
        }}>
        Change to en
      </button>
      <button
        onClick={() => {
          i18n.changeLanguage('vi');
        }}>
        Change to vi
      </button>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        <h1 className='App-title'>{t('welcome.title', { framework: 'react-i18next' })}</h1>

        <Trans t={t}>welcome.title</Trans>
        <br></br>
        
      </header>
    </div>
  );
}

// wrap component with withTranslation HOC you can access t , i18n via props
// 'common' is the namespace , when use t('key') will look up key from namespace
// when have multi namspace declare in a string array like withTranslation(['common','formal'])
// And access in t('formal:key')
export default withTranslation('common')(App);
