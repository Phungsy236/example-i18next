import { initReactI18next } from 'react-i18next';
import i18n from 'i18next';
import common_vi from '../src/translations/vi/common.json';
import common_en from '../src/translations/en/common.json';
i18n.use(initReactI18next).init({
  debug: true,
  interpolation: { escapeValue: false },
  // react: { useSuspense: false },
  lng: 'vi', // default lang to use
  resources: {
    vi: {
      common: common_vi, // common is the name space , common_vi is the source for namespace
    },
    en: {
      common: common_en,
    },
  },
});

export default i18n;
